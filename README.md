# About
Template surat permohonan Praktik Kerja Lapangan.

# Requirement
* Python 2.7.x
* Django 1.10.x, 1.11.x

# Setup
* Download [esp-master.zip](https://gitlab.com/HilmiZul/esp/-/archive/master/esp-master.zip) 
* Extract. Open Terminal.
* ```cd esp/```
* ```./manage.py makemigrations && ./manage.py migrate```
* ```./manage.py createsuperuser```
* ```./manage.py runserver```
* Go to ```http://localhost:8000```
